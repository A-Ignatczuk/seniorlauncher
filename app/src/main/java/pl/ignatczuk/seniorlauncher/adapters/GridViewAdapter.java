package pl.ignatczuk.seniorlauncher.adapters;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pl.ignatczuk.seniorlauncher.models.AppDetail;
import pl.ignatczuk.seniorlauncher.R;

/**
 * Adapter class for main menu activity. Required by GridView
 */
public class GridViewAdapter extends BaseAdapter{

    private Context mContext;
    private List<AppDetail> apps;

    public GridViewAdapter(Context context, List<AppDetail> apps){
        mContext = context;
        this.apps = apps;
    }

    @Override
    public int getCount(){
        return apps.size();
    }

    @Override
    public Object getItem(int i){
        return null;
    }

    @Override
    public long getItemId(int i){
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent){
        View gridViewAndroid;

        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gridViewAndroid = inflater.inflate(R.layout.gridview_layout, parent, false);
        }
        else{
            gridViewAndroid = convertView;
        }
        TextView textViewAndroid = gridViewAndroid.findViewById(R.id.android_gridview_text);
        ImageView imageViewAndroid = gridViewAndroid.findViewById(R.id.android_gridview_image);
        textViewAndroid.setText(apps.get(i).getLabel());
        imageViewAndroid.setImageDrawable(apps.get(i).getIcon());
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        imageViewAndroid.getLayoutParams().height = Math.round(metrics.heightPixels) / 7;
        textViewAndroid.setTextSize((metrics.widthPixels/metrics.scaledDensity) / 14);
        return gridViewAndroid;
    }
}
