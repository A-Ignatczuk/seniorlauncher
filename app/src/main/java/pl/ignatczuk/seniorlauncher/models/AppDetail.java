package pl.ignatczuk.seniorlauncher.models;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

import java.util.Objects;

/**
 * Installed application model
 */
public class AppDetail{
    private CharSequence label;
    private CharSequence name;
    private Drawable icon;

    public AppDetail(CharSequence label, CharSequence name, Drawable icon){
        this.label = label;
        this.name = name;
        this.icon = icon;
    }

    public CharSequence getLabel(){
        return label;
    }

    public void setLabel(CharSequence label){
        this.label = label;
    }

    public CharSequence getName(){
        return name;
    }

    public Drawable getIcon(){
        return icon;
    }

    public void setIcon(Drawable icon){
        this.icon = icon;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppDetail appDetail = (AppDetail) o;
        return Objects.equals(label, appDetail.label) &&
                Objects.equals(name, appDetail.name) &&
                Objects.equals(icon, appDetail.icon);
    }

    @Override
    public int hashCode(){
        return Objects.hash(label, name, icon);
    }

    @NonNull
    @Override
    public String toString(){
        return "AppDetail{" +
                "label=" + label +
                ", name=" + name +
                ", icon=" + icon +
                '}';
    }
}
