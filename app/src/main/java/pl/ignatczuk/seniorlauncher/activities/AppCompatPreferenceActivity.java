package pl.ignatczuk.seniorlauncher.activities;

import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatDelegate;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A {@link android.preference.PreferenceActivity} which implements and proxies the necessary calls
 * to be used with AppCompat.
 */
public abstract class AppCompatPreferenceActivity extends PreferenceActivity{

    private AppCompatDelegate mDelegate;

    /**
     * Called when the activity is first created.
     * This is where all the activity setup works
     * like creating views, binding data etc.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState){
        getDelegate().installViewFactory();
        getDelegate().onCreate(savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    /**
     * Called when activity start-up is complete
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        getDelegate().onPostCreate(savedInstanceState);
    }

    public ActionBar getSupportActionBar(){
        return getDelegate().getSupportActionBar();
    }

    @NonNull
    @Override
    public MenuInflater getMenuInflater(){
        return getDelegate().getMenuInflater();
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID){
        getDelegate().setContentView(layoutResID);
    }

    @Override
    public void setContentView(View view){
        getDelegate().setContentView(view);
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params){
        getDelegate().setContentView(view, params);
    }

    @Override
    public void addContentView(View view, ViewGroup.LayoutParams params){
        getDelegate().addContentView(view, params);
    }

    /**
     * Called when activity resume is complete
     */
    @Override
    protected void onPostResume(){
        super.onPostResume();
        getDelegate().onPostResume();
    }

    @Override
    protected void onTitleChanged(CharSequence title, int color){
        super.onTitleChanged(title, color);
        getDelegate().setTitle(title);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig){
        super.onConfigurationChanged(newConfig);
        getDelegate().onConfigurationChanged(newConfig);
    }

    /**
     * Called when an activity is no longer visible to the user.
     */
    @Override
    protected void onStop(){
        super.onStop();
        getDelegate().onStop();
    }

    /**
     * Performs any final cleanup before an activity is destroyed
     */
    @Override
    protected void onDestroy(){
        super.onDestroy();
        getDelegate().onDestroy();
    }

    public void invalidateOptionsMenu(){
        getDelegate().invalidateOptionsMenu();
    }

    private AppCompatDelegate getDelegate(){
        if (mDelegate == null) {
            mDelegate = AppCompatDelegate.create(this, null);
        }
        return mDelegate;
    }
}
