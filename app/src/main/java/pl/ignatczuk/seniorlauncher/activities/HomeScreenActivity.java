package pl.ignatczuk.seniorlauncher.activities;

import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.hardware.Camera;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import pl.ignatczuk.seniorlauncher.adapters.GridViewAdapter;
import pl.ignatczuk.seniorlauncher.models.AppDetail;
import pl.ignatczuk.seniorlauncher.utils.FlashLight;
import pl.ignatczuk.seniorlauncher.utils.GsmSignalUtil;
import pl.ignatczuk.seniorlauncher.utils.MainScreenUtil;
import pl.ignatczuk.seniorlauncher.utils.MyBatteryStateListener;
import pl.ignatczuk.seniorlauncher.R;
import pl.ignatczuk.seniorlauncher.utils.SosUtil;

/**
 * Main screen activity class, calls all the necessary utils to
 * build entire main screen activity
 */
public class HomeScreenActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener{
    //all view definitions
    private List<AppDetail> mainMenuButtons = new ArrayList<>();
    private HomeScreenActivity instance;
    private TextView carrier;
    private TextView txtBattery;
    private TextView signalStr;
    private ImageView signalIcon;
    private ImageView batteryIcon;
    private TextView signalStatus;
    private TextView batteryProc;
    private GridViewAdapter mainMenu;
    private ConstraintLayout mainMenuButton1;
    private ConstraintLayout mainMenuButton2;
    private ConstraintLayout mainMenuButton3;
    private ConstraintLayout mainMenuButton4;
    private ConstraintLayout mainMenuButton5;
    private ConstraintLayout mainMenuButton6;

    //static helper strings
    private static final String DIALER = "dialer";
    private static final String SMS = "sms";
    private static final String SOS = "sos";
    private static final String LIGHTER = "lighter";
    private static final String CLOCK = "clock";
    private static final String APP_DRAWER = "appdrawer";
    private static final String MISSED_SWITCH = "missed_switch";

    //all the used utils
    private SharedPreferences prefs;
    private Camera camera = null;
    private Camera.Parameters params;
    private FlashLight flashLight;
    private SosUtil sosUtil;
    private GsmSignalUtil gsmSignalUtil;
    private MainScreenUtil mainScreenUtil;
    private MyBatteryStateListener mBatteryLevelReceiver;

    //getters for all necessary fields
    public ConstraintLayout getMainMenuButton1(){
        return mainMenuButton1;
    }
    public ConstraintLayout getMainMenuButton2(){
        return mainMenuButton2;
    }
    public ConstraintLayout getMainMenuButton3(){
        return mainMenuButton3;
    }
    public ConstraintLayout getMainMenuButton4(){
        return mainMenuButton4;
    }
    public ConstraintLayout getMainMenuButton5(){
        return mainMenuButton5;
    }
    public ConstraintLayout getMainMenuButton6(){
        return mainMenuButton6;
    }

    public HomeScreenActivity getInstance(){
        return instance;
    }

    public TextView getSignalStr(){
        return signalStr;
    }

    public TextView getSignalStatus(){
        return signalStatus;
    }

    public TextView getCarrierView(){
        return carrier;
    }

    public ImageView getSignalIconView(){
        return signalIcon;
    }

    public ImageView getBatteryIcon(){
        return batteryIcon;
    }

    public TextView getBatteryProc(){
        return batteryProc;
    }

    public TextView getTxtBattery(){
        return txtBattery;
    }

    public List<AppDetail> getMainMenuButtons(){
        return mainMenuButtons;
    }

    public GridViewAdapter getMainMenu(){
        return mainMenu;
    }

    public SharedPreferences getPrefs(){
        return prefs;
    }

    public Camera getCamera(){
        return camera;
    }

    public void setCamera(Camera camera){
        this.camera = camera;
    }

    public Camera.Parameters getParams(){
        return params;
    }

    public void setParams(Camera.Parameters params){
        this.params = params;
    }

    public GsmSignalUtil getGsmSignalUtil(){
        return gsmSignalUtil;
    }

    public MyBatteryStateListener getmBatteryLevelReceiver(){
        return mBatteryLevelReceiver;
    }

    /**
     * Called when the activity is first created.
     * This is where all the activity setup works
     * like creating views, binding data etc.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (Objects.equals(getPrefs().getString("app_theme", "dark"), "light")){
            setTheme(R.style.AppThemeLight); //set light theme if selected in settings
        }
        instance = this;
        initUtils();
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //make activity fullscreen
        if (getSupportActionBar() != null){
            getSupportActionBar().hide(); // hide the title bar
        }
        setContentView(R.layout.activity_homescreen);
        initialize();
        loadList();
        loadMainMenu();
        getPrefs().registerOnSharedPreferenceChangeListener(this);
        if (getPrefs().getBoolean("first_run", true)){
            mainScreenUtil.firstRun();
            getPrefs().edit().putBoolean("first_run", false).apply();
        }
    }

    /**
     * Called when the activity will start interacting with the user.
     * Activity is at the top of the activity. Always followed by onPause()
     */
    @Override
    public void onResume(){
        super.onResume();
        mBatteryLevelReceiver.batteryLevel();
        refreshIcons();
        gsmSignalUtil.switchService(true);
    }

    /**
     * Called when the system is about to start resuming a previous activity.
     * Followed by either onResume() if the activity returns back to the front
     * or onStop() if it becomes invisible to the user
     */
    @Override
    public void onPause(){
        super.onPause();
        unregisterReceiver(mBatteryLevelReceiver);
        if (Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M && camera != null){
            camera.release();
            camera = null;
        }
        gsmSignalUtil.switchService(false);

    }

    /**
     * Called when app theme preference changes to trigger theme change immediately
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key){
        if (key.equals("app_theme")){
            recreate();
        }
    }

    /**
     * Initializes all the necessary views
     */
    private void initialize(){
        mainMenuButton1 = findViewById(R.id.desktop_icon1);
        mainMenuButton2 = findViewById(R.id.desktop_icon2);
        mainMenuButton3 = findViewById(R.id.desktop_icon3);
        mainMenuButton4 = findViewById(R.id.desktop_icon4);
        mainMenuButton5 = findViewById(R.id.desktop_icon5);
        mainMenuButton6 = findViewById(R.id.desktop_icon6);
        mainMenu = new GridViewAdapter(getInstance(), mainMenuButtons);
        batteryIcon = findViewById(R.id.battery_icon);
        signalIcon = findViewById(R.id.network_icon);
        batteryProc = findViewById(R.id.battery_proc);
        txtBattery = findViewById(R.id.battery);
        signalStr = findViewById(R.id.network);
        signalStatus = findViewById(R.id.network_status);
        signalStr.setRotation(180);
        txtBattery.setRotation(180);
        carrier = findViewById(R.id.carrier);
        carrier.setSelected(true);
        TextClock time = findViewById(R.id.clock);
        TextClock date = findViewById(R.id.date);
        date.setFormat12Hour(mainScreenUtil.dateFormatter());
        date.setFormat24Hour(mainScreenUtil.dateFormatter());
        time.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) (carrier.getTextSize()*2.5));
        date.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) (carrier.getTextSize()*1.2));
    }

    /**
     * Init all the necessary util classes
     */
    private void initUtils(){
        mBatteryLevelReceiver = new MyBatteryStateListener(getInstance());
        mainScreenUtil = new MainScreenUtil(getInstance());
        flashLight = new FlashLight(getInstance());
        sosUtil = new SosUtil(getInstance());
        gsmSignalUtil = new GsmSignalUtil(getInstance());
    }

    /**
     * Loads main menu buttons list
     */
    private void loadList(){
        TypedArray[] typedArray = new TypedArray[]{obtainStyledAttributes(new int[]{R.attr.dialer_icon}),
                obtainStyledAttributes(new int[] {R.attr.sms_icon}),
                obtainStyledAttributes(new int[] {R.attr.sos_icon}),
                obtainStyledAttributes(new int[] {R.attr.torch_icon}),
                obtainStyledAttributes(new int[] {R.attr.clock_icon}),
                obtainStyledAttributes(new int[] {R.attr.app_drawer_icon})};
        getMainMenuButtons().add(new AppDetail(getString(R.string.dialer),DIALER, typedArray[0].getDrawable(0)));
        getMainMenuButtons().add(new AppDetail(getString(R.string.sms),SMS, typedArray[1].getDrawable(0)));
        getMainMenuButtons().add(new AppDetail(getString(R.string.sos),SOS, typedArray[2].getDrawable(0)));
        getMainMenuButtons().add(new AppDetail(getString(R.string.torch),LIGHTER, typedArray[3].getDrawable(0)));
        getMainMenuButtons().add(new AppDetail(getString(R.string.clock),CLOCK, typedArray[4].getDrawable(0)));
        getMainMenuButtons().add(new AppDetail(getString(R.string.app_drawer),APP_DRAWER, typedArray[5].getDrawable(0)));
        for (TypedArray t: typedArray){
            t.recycle();
        }
    }

    /**
     * Sets adapter for main menu gridView and onClickListeners
     */

    private void setMainMenuButton(ConstraintLayout mainMenuButton, int num){
        List<AppDetail> apps = getMainMenuButtons();
        ImageView imageView = (ImageView) mainMenuButton.getViewById(R.id.android_gridview_image);
        imageView.setImageDrawable(apps.get(num-1).getIcon());
        TextView textView = (TextView) mainMenuButton.getViewById(R.id.android_gridview_text);
        textView.setText(apps.get(num-1).getLabel());
        mainMenuButton.setOnClickListener(v -> {
            switch (num){
                case 1:
                    mainScreenUtil.openDialer();
                    break;
                case 2:
                    mainScreenUtil.openSms();
                    break;
                case 3:
                    sosUtil.sosMainFunction();
                    break;
                case 4:
                    flashLight.flashlightSwitcher(num-1);
                    break;
                case 5:
                    mainScreenUtil.openAlarm();
                    break;
                case 6:
                    mainScreenUtil.openAppDrawer();
                    break;
                default:
                    Toast.makeText(getInstance(), getString(R.string.error_toast), Toast.LENGTH_LONG).show();
                    break;
            }
        });
    }

    public void loadMainMenu(){
        setMainMenuButton(getMainMenuButton1(), 1);
        setMainMenuButton(getMainMenuButton2(), 2);
        setMainMenuButton(getMainMenuButton3(), 3);
        setMainMenuButton(getMainMenuButton4(), 4);
        setMainMenuButton(getMainMenuButton5(), 5);
        setMainMenuButton(getMainMenuButton6(), 6);
    }

    /**
     * Responsible for icons refreshing, when
     * some action like new missed call or new SMS triggers
     */
    public void refreshIcons(){
        TypedArray[] typedArray =
                new TypedArray[]{obtainStyledAttributes(new int[] {R.attr.dialer_icon}),
                        obtainStyledAttributes(new int[] {R.attr.sms_icon})};
        for (AppDetail detail : getMainMenuButtons()){
            if (detail.getName().toString().equals(DIALER)){
                if (getPrefs().getBoolean(MISSED_SWITCH, false)){
                    mainScreenUtil.refreshDialer(detail);
                }
                else{
                    detail.setLabel(detail.getLabel().toString().split("\\(")[0]);
                    detail.setIcon(typedArray[0].getDrawable(0));
                }
            }
            if (detail.getName().toString().equals(SMS)){
                if (getPrefs().getBoolean(MISSED_SWITCH, false)){
                    mainScreenUtil.refreshSms(detail);
                }
                else{
                    detail.setLabel(detail.getLabel().toString().split("\\(")[0]);
                    detail.setIcon(typedArray[1].getDrawable(0));
                }
            }
        }
        loadMainMenu();
        for (TypedArray t : typedArray){
            t.recycle();
        }
    }
}