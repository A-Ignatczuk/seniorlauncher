package pl.ignatczuk.seniorlauncher.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.SwitchPreference;
import android.support.v7.app.ActionBar;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;

import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import pl.ignatczuk.seniorlauncher.BuildConfig;
import pl.ignatczuk.seniorlauncher.fragments.EditTextPreference;
import pl.ignatczuk.seniorlauncher.utils.PermissionsChecker;
import pl.ignatczuk.seniorlauncher.R;

import static pl.ignatczuk.seniorlauncher.utils.Report.reportViaMail;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 */
public class SettingsActivity extends AppCompatPreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    //helper strings
    private static final String SOS_SWITCH = "sos_switch";
    private static final String MISSED_SWITCH = "missed_switch";
    private static final String APP_THEME = "app_theme";
    private static final String SOS_MODE = "sos_mode";
    private static final String SOS_NAME = "sos_name";
    private static final String SOS_NUMBER = "sos_number";

    //permissions list
    private static String[] sosPermissions = {Manifest.permission.CALL_PHONE, Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE};
    private static String[] missedIndicatorPermissions = {Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_SMS};
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = (preference, value) -> {
        String stringValue = value.toString();

        if (preference instanceof ListPreference) {
            // For list preferences, look up the correct display value in
            // the preference's 'entries' list.
            ListPreference listPreference = (ListPreference) preference;
            int index = listPreference.findIndexOfValue(stringValue);

            // Set the summary to reflect the new value.
            preference.setSummary(
                    index >= 0
                            ? listPreference.getEntries()[index]
                            : null);

        } else if (preference instanceof EditTextPreference){
            String str=  "(^\\s?((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?\\s?)|(911|112)";
            EditTextPreference editText = (EditTextPreference) preference;
            preference.setSummary(editText.getEditText().getText());
            if (preference.getKey().equals(SOS_NUMBER) &&
                    !Pattern.compile(str).matcher(stringValue).matches() &&
                    (stringValue.length()) != 0){
                showNotMatchingPhoneAlertDialog(preference);
            }
        } else if (preference instanceof SwitchPreference){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (preference.getKey().equals(SOS_SWITCH) && !((SwitchPreference) preference).isChecked()) {
                    PermissionsChecker.askForPermission(preference.getContext(), (Activity) preference.getContext(), sosPermissions);
                }
                if (preference.getKey().equals(MISSED_SWITCH) && !((SwitchPreference) preference).isChecked()) {
                    PermissionsChecker.askForPermission(preference.getContext(), (Activity) preference.getContext(), missedIndicatorPermissions);
                }
            }

        } else {
            // For all other preferences, set the summary to the value's
            // simple string representation.
            preference.setSummary(stringValue);
        }
        return true;
    };

    /**
     * Helper method that clears EditTextPreference when wrong value was entered
     * @param preference preference element
     * @return SharedPreference commit status
     */
    private static boolean clearPrefs(Preference preference) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(preference.getContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preference.getKey(), "");
        preference.setSummary("");
        ((EditTextPreference) preference).setText("");
        return editor.commit();
    }

    /**
     * Show an AlertDialog when user inputs a wrong phone number
     */
    private static void showNotMatchingPhoneAlertDialog(Preference preference){
        android.app.AlertDialog alert = new android.app.AlertDialog.Builder(preference.getContext())
                .create();
        alert.setTitle(preference.getContext().getString(R.string.phone_error_title));
        alert.setMessage(preference.getContext().getString(R.string.phone_error_text));
        alert.setButton(DialogInterface.BUTTON_POSITIVE, preference.getContext().getString(R.string.dialog_ok), (dialog, which) -> {
            clearPrefs(preference);
            dialog.cancel();
        });
        alert.show();
    }


    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        if (!preference.getKey().equals(MISSED_SWITCH) && !preference.getKey().equals(SOS_SWITCH)){
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.getContext())
                            .getString(preference.getKey(), ""));
        }
    }

    /**
     * Called when the activity is first created.
     * This is where all the activity setup works
     * like creating views, binding data etc.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (Objects.equals(prefs.getString(APP_THEME, "dark"), "light")){
            setTheme(R.style.AppThemeLight);
        }
        super.onCreate(savedInstanceState);
        prefs.registerOnSharedPreferenceChangeListener(this);
        setupActionBar();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Called when a settings menu item has been selected by the user.
     */
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!super.onMenuItemSelected(featureId, item)) {
                NavUtils.navigateUpFromSameTask(this);
            }
            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || GeneralPreferenceFragment.class.getName().equals(fragmentName)
                || AboutAppFragment.class.getName().equals(fragmentName);
    }

    /**
     * Called when app theme preference changes to trigger theme change immediately
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(APP_THEME))
        {
            recreate();
        }
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        /**
         * Called when the activity is first created.
         * This is where all the activity setup works
         * like creating views, binding data etc.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);

            // Bind the summaries of preferences to their values.
            // When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference(MISSED_SWITCH));
            bindPreferenceSummaryToValue(findPreference(SOS_SWITCH));
            bindPreferenceSummaryToValue(findPreference(APP_THEME));
            bindPreferenceSummaryToValue(findPreference(SOS_MODE));
            bindPreferenceSummaryToValue(findPreference(SOS_NAME));
            bindPreferenceSummaryToValue(findPreference(SOS_NUMBER));
        }

        /**
         * Called when the activity will start interacting with the user.
         * Activity is at the top of the activity. Always followed by onPause()
         */
        @Override
        public void onResume(){
            super.onResume();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if ((!PermissionsChecker.checkPermissions(getContext(), sosPermissions))) {
                    SwitchPreference switchPreference = (SwitchPreference) findPreference(SOS_SWITCH);
                    switchPreference.setChecked(false);
                }
                if ((!PermissionsChecker.checkPermissions(getContext(), missedIndicatorPermissions))) {
                    SwitchPreference switchPreference = (SwitchPreference) findPreference(MISSED_SWITCH);
                    switchPreference.setChecked(false);
                }
            }
        }

        /**
         * Called when a settings item has been selected by the user.
         */
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This fragment is for info purposes only
     * It is used to show app version, author and button for feedback
     */
    public static class AboutAppFragment extends PreferenceFragment {
        /**
         * Called when the activity is first created.
         * This is where all the activity setup works
         * like creating views, binding data etc.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_about);
            Preference contactMe = findPreference("pref_author_mail");
            Preference bug = findPreference("pref_app_version");
            bug.setSummary(getString(R.string.version) + BuildConfig.VERSION_NAME);
//            bug.setOnPreferenceClickListener(preference -> testReport());
            contactMe.setOnPreferenceClickListener(preference -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:"
                        + "ignatczuk@gmail.com"
                        + "?subject=" + "Feedback" + "&body=" + "");
                intent.setData(data);
                startActivity(intent);
                return true;
            });
        }

        /**
         * Called when a settings item has been selected by the user.
         */
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        /**
         * Function that throws an exception. For demo purposes only
         * @return true
         */
        private boolean testReport(){
            try{
                throw new NoSuchMethodException(ACTIVITY_SERVICE);
            }
            catch(NoSuchMethodException e){
                reportViaMail(getActivity(), e);
                return true;
            }
        }
    }
}
