package pl.ignatczuk.seniorlauncher.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;

import java.util.Objects;

import pl.ignatczuk.seniorlauncher.utils.LoadAppsListThread;
import pl.ignatczuk.seniorlauncher.R;

/**
 * An activity that presents all the installed applications on the device.
 * Top toolbar has a button, that opens {@link SettingsActivity}
 */
public class AppDrawerActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

    /**
     * Adds items to the action bar menu if it's present
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.settings_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Handles action bar menu items selection
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_settings:
                Intent intent = new Intent (this, SettingsActivity.class);
                startActivity(intent);
                break;
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * Called when the activity is first created.
     * This is where all the activity setup works
     * like creating views, binding data etc.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);
        super.onCreate(savedInstanceState);
        if (Objects.equals(prefs.getString("app_theme", "dark"), "light")){
            setTheme(R.style.AppThemeLight);
        }
        setContentView(R.layout.activity_appdrawer);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        GridView androidGridView = findViewById(R.id.grid_view_image_text);
        new LoadAppsListThread(AppDrawerActivity.this, androidGridView).execute();
    }

    /**
     * Called when app theme preference changes to trigger theme change immediately
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key){
        if (key.equals("app_theme")){
            recreate();
        }
    }
}
