package pl.ignatczuk.seniorlauncher.fragments;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Custom implementation of EditTextPreference
 */
public class EditTextPreference extends android.preference.EditTextPreference{
    public EditTextPreference(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
    }

    public EditTextPreference(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    public EditTextPreference(Context context){
        super(context);
    }

    /**
     * Set summary on EditTextPreference with current value
     * @return CharSequence of summary
     */
    @Override
    public CharSequence getSummary(){
        return getText();
    }
}
