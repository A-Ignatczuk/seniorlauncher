package pl.ignatczuk.seniorlauncher.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import java.io.PrintWriter;
import java.io.StringWriter;

import pl.ignatczuk.seniorlauncher.R;

/**
 * Helper class for sending reports to developer when some kind of error occurred
 */
public class Report {
    private Report(){
        //nothing here
    }

    /**
     * Show an AlertDialog to user informing that some kind of error occurred and ask for sending a stack trace
     * @param instance application context
     * @param e error stack trace
     */
    public static void reportViaMail(Context instance, Exception e){
        new AlertDialog.Builder(instance)
                .setTitle(R.string.error_title)
                .setMessage(R.string.error_text)
                .setPositiveButton(R.string.question_positive, (dialog, which) -> {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    StringWriter sw = new StringWriter();
                    PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace(pw);
                    Uri data = Uri.parse("mailto:"
                            + "ignatczuk@gmail.com"
                            + "?subject=" + "Bug report for app " + instance.getResources().getString(R.string.app_name) + "&body=" + "Android SDK: " + Build.VERSION.SDK_INT + " (" + Build.VERSION.RELEASE +")\n\n" + sw.toString());
                    intent.setData(data);
                    instance.startActivity(intent);
                })
                .setNegativeButton(R.string.question_negative, (dialog1, which1) -> dialog1.dismiss())
                .show();
    }
}
