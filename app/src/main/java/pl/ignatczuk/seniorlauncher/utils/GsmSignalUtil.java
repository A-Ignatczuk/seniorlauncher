package pl.ignatczuk.seniorlauncher.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import pl.ignatczuk.seniorlauncher.R;
import pl.ignatczuk.seniorlauncher.activities.HomeScreenActivity;

/**
 * Responsible for mobile network actions util class
 */
public class GsmSignalUtil{

    private HomeScreenActivity instance;

    private TelephonyManager mTelephonyManager;

    private MyPhoneStateListener mPhoneStateListener;

    public GsmSignalUtil(HomeScreenActivity instance){
        this.instance = instance;
        mPhoneStateListener = new MyPhoneStateListener(instance);
        mTelephonyManager = (TelephonyManager) instance.getSystemService(Context.TELEPHONY_SERVICE);
        mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_SERVICE_STATE | PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }

    public void switchService(boolean state){
        if (state){
            mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_SERVICE_STATE | PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        }
        else{
            mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
        }
    }

    /**
     * Checks if sim is inserted and working
     * @return sim state
     */
    boolean simState(){
        TelephonyManager telMgr = (TelephonyManager) instance.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                setFragments(R.string.no_sim_state, "", "", R.attr.no_signal_icon);
                return false;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                setFragments(R.string.sim_locked_status, "", "", R.attr.no_signal_icon);
                return false;
            case TelephonyManager.SIM_STATE_NOT_READY:
                setFragments(R.string.sim_not_ready_status, "", "", R.attr.no_signal_icon);
                return false;
            case TelephonyManager.SIM_STATE_READY:
                return true;
            default:
                return false;
        }
    }

    /**
     * Checks network type, airplane mode state and roaming state
     * @return type of mobile network and roaming status
     */
    public String getNetworkClass(){
        String ret = "";
        if (isAirplaneModeOn()){
            setFragments(R.string.airplane_mode, "", "", R.attr.no_signal_icon);
            return "";
        }
        if (simState()){
            if (mTelephonyManager.isNetworkRoaming()){
                ret = "(R)";
            }
            int networkType = mTelephonyManager.getNetworkType();

            if (getCarrier().isEmpty()){
                instance.getCarrierView().setText(instance.getString(R.string.sim_status_connecting));
                return "";
            }
            switch (networkType){
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return "G" + ret;
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return "E" + ret;
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "2G" + ret;
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                    return "3G" + ret;
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return "H" + ret;
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return "H+" + ret;
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return "LTE" + ret;
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                    return "";
                default:
                    return "";
            }
        }
        if (isAirplaneModeOn()){
            setFragments(R.string.airplane_mode, "", "", R.attr.no_signal_icon);
            return "";
        }
        return "";

    }

    /**
     * Returns carrier name
     * @return carrier name
     */
    String getCarrier(){
        return mTelephonyManager.getNetworkOperatorName();
    }

    /**
     * Checks airplane mode state
     * @return airplane mode state
     */
    private boolean isAirplaneModeOn(){
        return Settings.Global.getInt(instance.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    /**
     * Sets all views that shows network state
     */
    void setFragments(int carrierView, String signalStr, String signalStatus, int drawableIcon){
        TypedArray typedArray = instance.obtainStyledAttributes(new int[]{drawableIcon});
        instance.getCarrierView().setText(instance.getString(carrierView));
        instance.getSignalStr().setText(signalStr);
        instance.getSignalStatus().setText(signalStatus);
        instance.getSignalIconView().setImageDrawable(typedArray.getDrawable(0));
        typedArray.recycle();
    }

    /**
     * Sets all views that shows network state
     */
    void setFragments(int carrierView, int drawableIcon){
        TypedArray typedArray = instance.obtainStyledAttributes(new int[]{drawableIcon});
        instance.getCarrierView().setText(instance.getString(carrierView));
        instance.getSignalIconView().setImageDrawable(typedArray.getDrawable(0));
        typedArray.recycle();
    }
}
