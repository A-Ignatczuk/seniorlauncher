package pl.ignatczuk.seniorlauncher.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper util to request permissions from user
 */
public final class PermissionsChecker {

    private PermissionsChecker(){
        //not called
    }

    /**
     * Checks if permission is already granted
     * and returns permissions that are not granted.
     * Working only on Android 6.0 and higher.
     * @param context activity context
     * @param permissions required permissions list
     * @return list of missing permissions
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    private static List<String> getPermissionsReq(Context context, String[] permissions){
        List<String> permissionsToRequest = new ArrayList<>();
        for (String permission : permissions){
            if (ContextCompat.checkSelfPermission(context, permission)
                    != PackageManager.PERMISSION_GRANTED){
                permissionsToRequest.add(permission);
            }
        }
        return permissionsToRequest;
    }

    /**
     * Retrieve missing permissions list and ask user for them
     * @param context activity context
     * @param activity activity
     * @param neededPermissions missing permissions list
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void askForPermission(Context context, Activity activity, String[] neededPermissions){
        List<String> permissionsToRequest = getPermissionsReq(context, neededPermissions);
        if (!permissionsToRequest.isEmpty()){
            activity.requestPermissions(permissionsToRequest.toArray(new String[0]), 1);
        }
    }

    /**
     * Checks if permissions passed as argument are granted by user
     * @param context activity context
     * @param neededPermissions list of permissions to check
     * @return if there are any permissions that are not granted
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean checkPermissions(Context context, String[] neededPermissions){
        for (String neededPermission : neededPermissions){
            if (ContextCompat.checkSelfPermission(context, neededPermission)
                    != PackageManager.PERMISSION_GRANTED){
                return false;
            }
        }
        return true;
    }
}
