package pl.ignatczuk.seniorlauncher.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import pl.ignatczuk.seniorlauncher.R;
import pl.ignatczuk.seniorlauncher.adapters.GridViewAdapter;
import pl.ignatczuk.seniorlauncher.models.AppDetail;

/**
 * AsyncTask class, that loads applications list in background
 */
public class LoadAppsListThread extends AsyncTask<Void, Void, String>{

    private final AtomicReference<Context> mContext = new AtomicReference<>();

    private PackageManager manager;

    private List<AppDetail> apps = new ArrayList<>();

    private final AtomicReference<GridView> androidGridView = new AtomicReference<>();

    private ProgressDialog progressDialog;

    public LoadAppsListThread(Context context, GridView androidGridView){
        mContext.set(context);
        manager = mContext.get().getPackageManager();
        this.androidGridView.set(androidGridView);
    }

    /**
     * Loads application list on background
     * @param params params
     * @return if process was ok
     */
    @Override
    protected String doInBackground(Void... params){
        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
        Collections.sort(availableActivities, new ResolveInfo.DisplayNameComparator(manager));
        for(ResolveInfo ri:availableActivities){
            if (!ri.activityInfo.packageName.equals("pl.ignatczuk.seniorlauncher")){
                apps.add(new AppDetail(ri.loadLabel(manager),ri.activityInfo.packageName, ri.activityInfo.loadIcon(manager)));
            }
        }
        return "OK";

    }

    /**
     * Shows ProgressDialog before list loading
     */
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
        if (progressDialog == null){
            progressDialog = new ProgressDialog(mContext.get());
            progressDialog.setMessage(mContext.get().getString(R.string.app_drawer_wait));
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
    }

    /**
     * Show loaded application list on app drawer activity
     */
    private void loadList(){
        GridViewAdapter adapterViewAndroid = new GridViewAdapter(mContext.get(), apps);
        androidGridView.get().setAdapter(adapterViewAndroid);
        androidGridView.get().setOnItemClickListener((parent, view, pos, id) -> {
            Intent i = manager.getLaunchIntentForPackage(apps.get(pos).getName().toString());
            mContext.get().startActivity(i);
        });
    }

    /**
     * Dismiss ProgressDialog after apps list loaded
     */
    @Override
    protected void onPostExecute(String result){
        super.onPostExecute(result);
        loadList();
        if (progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }
}
