package pl.ignatczuk.seniorlauncher.utils;

import android.os.Build;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;

import pl.ignatczuk.seniorlauncher.R;
import pl.ignatczuk.seniorlauncher.activities.HomeScreenActivity;

/**
 * Listens for network state and returns this information to main activity
 */
public class MyPhoneStateListener extends PhoneStateListener{

    private HomeScreenActivity instance;

    MyPhoneStateListener(HomeScreenActivity instance){
        this.instance = instance;
    }

    /**
     * Called when mobile network signal strength changes.
     * Sets network strength view on main screen.
     * Divided to 2 parts to provide support for older versions of android.
     * @param signalStrength current signal strength
     */
    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength){
        String level;
        String lvl1 = "█\n\n \n\n  \n\n   ";
        String lvl2 = "█\n\n█\n\n  \n\n   ";
        String lvl3 = "█\n\n█\n\n██\n\n   ";
        String lvl4 = "█\n\n█\n\n██\n\n███";

        super.onSignalStrengthsChanged(signalStrength);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            int signalLvl = signalStrength.getLevel();
            switch (signalLvl) {
                case 1:
                    level = lvl1;
                    break;
                case 2:
                    level = lvl2;
                    break;
                case 3:
                    level = lvl3;
                    break;
                case 4:
                    level = lvl4;
                    break;
                default:
                    level = "";
                    break;
            }
        }

        else{
            int signalLvl = signalStrength.getGsmSignalStrength();

            if (signalLvl == 0 || signalLvl == 99){
                level = "";
            }
            else if (signalLvl <= 7){
                level = lvl1;
            }
            else if (signalLvl <= 15){
                level = lvl2;
            }
            else if (signalLvl <= 23){
                level = lvl3;
            }
            else if (signalLvl <= 31){
                level = lvl4;
            }
            else{
                level = "";
            }
        }
        instance.getSignalStr().setText(level);
        instance.getCarrierView().setText(instance.getGsmSignalUtil().getCarrier());
        instance.getSignalStatus().setText(instance.getGsmSignalUtil().getNetworkClass());
    }


    /**
     * Called when mobile network state changes.
     * Sets icon and current state (is network is on and connected).
     * @param serviceState current network state
     */
    @Override
    public void onServiceStateChanged(ServiceState serviceState){
        super.onServiceStateChanged(serviceState);
        if (instance.getGsmSignalUtil().simState()){
            switch (serviceState.getState()) {
                case ServiceState.STATE_EMERGENCY_ONLY:
                    instance.getGsmSignalUtil().setFragments(R.string.emergency_only, "", "", R.attr.no_signal_icon);
                    break;
                case ServiceState.STATE_OUT_OF_SERVICE:
                    instance.getGsmSignalUtil().setFragments(R.string.no_service, "", "", R.attr.no_signal_icon);
                    break;
                case ServiceState.STATE_IN_SERVICE:
                    instance.getGsmSignalUtil().setFragments(R.string.sim_status_connecting, R.attr.signal_icon);
                    break;
                default:
                    break;
            }
        }
        if (isAirplaneModeOn()){
            instance.getGsmSignalUtil().setFragments(R.string.airplane_mode, "", "", R.attr.no_signal_icon);
        }
    }

    /**
     * Checks if airplane is on
     * @return airplane mode state
     */
    private boolean isAirplaneModeOn(){
        return Settings.Global.getInt(instance.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }


}
