package pl.ignatczuk.seniorlauncher.utils;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.AlarmClock;
import android.provider.CallLog;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import java.text.Format;
import java.text.SimpleDateFormat;

import pl.ignatczuk.seniorlauncher.R;
import pl.ignatczuk.seniorlauncher.activities.AppDrawerActivity;
import pl.ignatczuk.seniorlauncher.activities.HomeScreenActivity;
import pl.ignatczuk.seniorlauncher.activities.SettingsActivity;
import pl.ignatczuk.seniorlauncher.models.AppDetail;

/**
 * Responsible for actions, used on main screen activity
 */
public class MainScreenUtil{

    private HomeScreenActivity instance;

    private static String[] missedIndicatorPermissions = {Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_SMS};

    private static final String SMS_URI = "content://sms/inbox";

    private static final String CALLS_URI = "content://call_log/calls";

    public MainScreenUtil(HomeScreenActivity instance){
        this.instance = instance;
    }

    /**
     * Shows AlertDialog on first run
     */
    public void firstRun(){
        new AlertDialog.Builder(instance)
                .setTitle(R.string.first_run_title)
                .setMessage(R.string.first_run_message)
                .setPositiveButton(R.string.question_positive, (dialog, which) -> {
                    Intent intent = new Intent (instance, SettingsActivity.class);
                    instance.startActivity(intent);
                })
                .setNegativeButton(R.string.question_negative, (dialog1, which1) -> dialog1.dismiss())
                .show();
    }

    /**
     * Formats date to show day of the week
     * @return formatted date
     */
    public String dateFormatter(){
        Format dateFormat = android.text.format.DateFormat.getDateFormat(instance.getApplicationContext());
        return "EEE | " + ((SimpleDateFormat) dateFormat).toLocalizedPattern();
    }

    /**
     * Retrieves information if there is any unread SMS
     * @return number of unread SMS
     */
    private int getMissedSms(){
        final Uri uri = Uri.parse(SMS_URI);

        Cursor cursor = instance.getContentResolver().query(
                uri,
                null,
                "read = 0",
                null,
                null);
        int result = cursor != null ? cursor.getCount() : 0;
        if (cursor != null){
            cursor.close();
        }
        return result;
    }

    /**
     * Retrieves information if there is any missed call
     * @return number of missed calls
     */
    private int getMissedCalls(){
        final Uri uri = Uri.parse(CALLS_URI);

        Cursor cursor = instance.getContentResolver().query(
                uri,
                null,
                CallLog.Calls.TYPE + "=?" + " and " + CallLog.Calls.IS_READ + "=?",
                new String[] { String.valueOf(CallLog.Calls.MISSED_TYPE), "0" },
                null);

        int result = cursor != null ? cursor.getCount() : 0;
        if (cursor != null){
            cursor.close();
        }
        return result;
    }

    /**
     * Opens default dialer
     */
    public void openDialer(){
        try {
            Intent i = new Intent(Intent.ACTION_DIAL);
            instance.startActivity(i);
        }
        catch(ActivityNotFoundException e){
            showAlertDialog();
        }
    }

    /**
     * Opens default SMS app
     */
    public void openSms(){
        String defaultApplication = Settings.Secure.getString(instance.getContentResolver(), "sms_default_application");
        PackageManager pm = instance.getPackageManager();
        Intent i = pm.getLaunchIntentForPackage(defaultApplication );
        if (i != null) {
            instance.startActivity(i);
        }
    }

    /**
     * Opens default alarm app
     */
    public void openAlarm(){
        try{
            Intent i = new Intent(AlarmClock.ACTION_SHOW_ALARMS);
            instance.startActivity(i);
        }
        catch(ActivityNotFoundException e){
                showAlertDialog();
        }
    }

    /**
     * Opens app drawer activity
     */
    public void openAppDrawer(){
        Intent intent = new Intent (instance, AppDrawerActivity.class);
        instance.startActivity(intent);
    }

    /**
     * Refresh dialer icon method for older versions of android
     * @param detail main menu element
     */
    private void refreshDialerCompatibility(AppDetail detail){
        TypedArray typedArray = instance.obtainStyledAttributes(new int[] {R.attr.dialer_icon});
        detail.setLabel(detail.getLabel().toString().split("\\(")[0] + (getMissedCalls() == 0 ? "" : "(" + getMissedCalls() + ")"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            detail.setIcon(getMissedCalls() == 0 ? typedArray.getDrawable(0) : instance.getDrawable(R.drawable.dialer_missed));
        }
        else{
            detail.setIcon(getMissedCalls() == 0 ? typedArray.getDrawable(0) : instance.getResources().getDrawable(R.drawable.dialer_missed));
        }
        typedArray.recycle();
    }

    /**
     * Refresh SMS icon method for older versions of android
     * @param detail main menu element
     */
    private void refreshSmsCompatibility(AppDetail detail){
        TypedArray typedArray = instance.obtainStyledAttributes(new int[] {R.attr.sms_icon});
        detail.setLabel(detail.getLabel().toString().split("\\(")[0] + (getMissedSms() == 0 ? "" : "(" + getMissedSms() + ")"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            detail.setIcon(getMissedSms() == 0 ? typedArray.getDrawable(0) : instance.getDrawable(R.drawable.sms_new));
        }
        else{
            detail.setIcon(getMissedSms() == 0 ? typedArray.getDrawable(0) : instance.getResources().getDrawable(R.drawable.sms_new));
        }
        typedArray.recycle();
    }

    /**
     * Refresh dialer icon method for newer versions of android (6.0 or higher)
     * @param detail main menu element
     */
    public void refreshDialer(AppDetail detail){
        TypedArray typedArray = instance.obtainStyledAttributes(new int[] {R.attr.dialer_icon});
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (PermissionsChecker.checkPermissions(instance, missedIndicatorPermissions)){
                detail.setLabel(detail.getLabel().toString().split("\\(")[0] + (getMissedCalls() == 0 ? "" : "(" + getMissedCalls() + ")"));
                detail.setIcon(getMissedCalls() == 0 ? typedArray.getDrawable(0) : instance.getDrawable(R.drawable.dialer_missed));
            }
        }
        else{
            refreshDialerCompatibility(detail);
        }
        typedArray.recycle();
    }

    /**
     * Refresh SMS icon method for newer versions of android (6.0 or higher)
     * @param detail main menu element
     */
    public void refreshSms(AppDetail detail){
        TypedArray typedArray = instance.obtainStyledAttributes(new int[] {R.attr.sms_icon});
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (PermissionsChecker.checkPermissions(instance, missedIndicatorPermissions)){
                detail.setLabel(detail.getLabel().toString().split("\\(")[0] + (getMissedSms() == 0 ? "" : "(" + getMissedSms() + ")"));
                detail.setIcon(getMissedSms() == 0 ? typedArray.getDrawable(0) : instance.getDrawable(R.drawable.sms_new));
            }
        }
        else{
            refreshSmsCompatibility(detail);
        }
        typedArray.recycle();
    }

    /**
     * Shows an AlertDialog when device missing a default app for some action
     */
    private void showAlertDialog(){
        android.app.AlertDialog alert = new android.app.AlertDialog.Builder(instance)
                .create();
        alert.setTitle(instance.getString(R.string.intent_error_title));
        alert.setMessage(instance.getString(R.string.intent_error_text));
        alert.setButton(DialogInterface.BUTTON_POSITIVE, instance.getString(R.string.dialog_ok), (dialog, which) -> dialog.cancel());
        alert.show();
    }
}
