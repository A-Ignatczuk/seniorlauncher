package pl.ignatczuk.seniorlauncher.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.os.BatteryManager;

import java.text.MessageFormat;

import pl.ignatczuk.seniorlauncher.R;
import pl.ignatczuk.seniorlauncher.activities.HomeScreenActivity;

/**
 * Listens for battery state and returns this information to main activity
 */
public class MyBatteryStateListener extends BroadcastReceiver{

    private HomeScreenActivity instance;

    String lvlSym;

    String lvl1 = "█\n\n \n\n  \n\n   ";
    String lvl2 = "█\n\n█\n\n  \n\n   ";
    String lvl3 = "█\n\n█\n\n██\n\n   ";
    String lvl4 = "█\n\n█\n\n██\n\n███";

    public MyBatteryStateListener(HomeScreenActivity instance){
        this.instance = instance;
    }

    /**
     * Called when battery level or state changes.
     * Sets icon and current state (is battery is charging or state unknown).
     * @param context activity context
     * @param intent intent
     */
    @Override
    public void onReceive(Context context, Intent intent){
        int rawLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        int level = -1;

        if (rawLevel >= 0 && scale > 0){
            level = (rawLevel * 100) / scale;
        }

        TypedArray[] typedArray = new TypedArray[]{instance.obtainStyledAttributes(new int[]{R.attr.battery_charging_icon}),
                instance.obtainStyledAttributes(new int[]{R.attr.battery_icon}),
                instance.obtainStyledAttributes(new int[]{R.attr.battery_unknown_icon}),
                instance.obtainStyledAttributes(new int[]{R.attr.battery_discharged})};

        switch (status){
            case BatteryManager.BATTERY_STATUS_CHARGING:
                instance.getBatteryIcon().setImageDrawable(typedArray[0].getDrawable(0));
                break;
            case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
            case BatteryManager.BATTERY_STATUS_DISCHARGING:
            case BatteryManager.BATTERY_STATUS_FULL:
                instance.getBatteryIcon().setImageDrawable(typedArray[1].getDrawable(0));
                break;
            case BatteryManager.BATTERY_STATUS_UNKNOWN:
                instance.getBatteryIcon().setImageDrawable(typedArray[2].getDrawable(0));
                break;
            default:
                break;
        }

        batteryLevelSetter(level, status, typedArray);

        instance.getTxtBattery().setText(lvlSym);
        instance.getBatteryProc().setText(MessageFormat.format("{0}%", level));
        instance.refreshIcons();

        for (TypedArray t : typedArray){
            t.recycle();
        }
    }

    /**
     * Registers battery level listener
     */
    public void batteryLevel(){
        IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        instance.registerReceiver(instance.getmBatteryLevelReceiver(), batteryLevelFilter);
    }

    private void batteryLevelSetter(int level, int status, TypedArray[] typedArray){
        if (level>=0 && level<=10){
            if (status == BatteryManager.BATTERY_STATUS_NOT_CHARGING ||
                    status == BatteryManager.BATTERY_STATUS_DISCHARGING){
                instance.getBatteryIcon().setImageDrawable(typedArray[3].getDrawable(0));
            }
            lvlSym = "";
        }
        else if (level>10 && level<=30){
            lvlSym = lvl1;
        }
        else if (level>30 && level <=50){
            lvlSym = lvl2;
        }
        else if (level>50 && level<=75){
            lvlSym = lvl3;
        }
        else if (level>75 && level<=100){
            lvlSym = lvl4;
        }
        else{
            lvlSym = "";
        }
    }
}
