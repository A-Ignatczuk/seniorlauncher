package pl.ignatczuk.seniorlauncher.utils;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.support.annotation.NonNull;

import pl.ignatczuk.seniorlauncher.R;
import pl.ignatczuk.seniorlauncher.activities.HomeScreenActivity;

/**
 * Responsible for flashlight actions util class
 */
public class FlashLight{
    private HomeScreenActivity instance;
    private boolean isOn = false;

    private boolean isFlashAvailable;

    @TargetApi(Build.VERSION_CODES.M)
    private CameraManager mCameraManager;

    /**
     * Check if camera is present (android 4.4 - android 5.1)
     * Check if flashlight is present (android 6.0 or greater) and register torchCallback
     * @param instance activity context
     */
    public FlashLight(HomeScreenActivity instance){
        this.instance = instance;
        isFlashAvailable = instance.getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
            mCameraManager = (CameraManager) instance.getSystemService(Context.CAMERA_SERVICE);
            CameraManager.TorchCallback torchCallback = new CameraManager.TorchCallback(){
                @Override
                public void onTorchModeChanged(@NonNull String cameraId, boolean enabled){
                    super.onTorchModeChanged(cameraId, enabled);
                    isOn = enabled;
                }
            };
            mCameraManager.registerTorchCallback(torchCallback, null);
        }
    }

    /**
     * Compatibility method, that triggers flashlight on android 4.4 - 5.1
     * Using Camera API
     * @param camera Camera API
     * @param params Camera API params
     * @return flashlight state
     */
    private boolean switchFlashLight(Camera camera, Camera.Parameters params){
        if (!isFlashAvailable){
            showNoFlashError();
            return false;
        }
        else{
            try {
                if (camera != null && params != null){
                    if (!isOn){
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                        camera.setParameters(params);
                        camera.startPreview();
                        isOn = true;
                        return true;
                    }
                    else{
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        camera.setParameters(params);
                        camera.stopPreview();
                        camera.release();
                        isOn = false;
                        return false;
                    }
                }
                return false;
            }
            catch (RuntimeException e){
                Report.reportViaMail(instance,e);
                return false;
            }
        }
    }

    /**
     * Main method, that triggers flashlight on android 6.0 or greater
     * Using Camera2 API
     * @return flashlight state
     */
    private boolean switchFlashLight(){
        if (!isFlashAvailable){
            showNoFlashError();
            return false;
        }
        else{
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
                try{
                    String mCameraId = mCameraManager.getCameraIdList()[0];
                    mCameraManager.setTorchMode(mCameraId, !isOn);
                    return !isOn;

                }
                catch (CameraAccessException e){
                    Report.reportViaMail(instance,e);
                    return false;
                }
            }
            return false;
        }
    }

    /**
     * Flashlight handler for older android versions using old Camera API
     * @param pos used to change icon on main screen
     */
    private void flashLightCompatibility(int pos){
        TypedArray typedArray = instance.obtainStyledAttributes(new int[] {R.attr.torch_icon});
        try{
            if (instance.getCamera() == null){
                instance.setCamera(Camera.open(0));
                instance.setParams(instance.getCamera().getParameters());
            }
            if(switchFlashLight(instance.getCamera(), instance.getParams())){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    instance.getMainMenuButtons().get(pos).setIcon(instance.getDrawable(R.drawable.torch_on));
                }
                else{
                    instance.getMainMenuButtons().get(pos).setIcon(instance.getResources().getDrawable(R.drawable.torch_on));
                }
                instance.loadMainMenu();
            }
            else{
                instance.getMainMenuButtons().get(pos).setIcon(typedArray.getDrawable(0));
                typedArray.recycle();
                instance.loadMainMenu();
                instance.setCamera(null);
            }
        }
        catch(RuntimeException e){
            Report.reportViaMail(instance,e);
        }
    }

    /**
     * Main flashlight handler for android 6.0 or higher using Camera2 API
     * @param pos used to change icon on main screen
     */
    public void flashlightSwitcher(int pos){
        TypedArray typedArray = instance.obtainStyledAttributes(new int[] {R.attr.torch_icon});
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M){
            if(switchFlashLight()){
                instance.getMainMenuButtons().get(pos).setIcon(instance.getDrawable(R.drawable.torch_on));
                instance.loadMainMenu();
            }
            else{
                instance.getMainMenuButtons().get(pos).setIcon(typedArray.getDrawable(0));
                instance.loadMainMenu();
            }
        }
        else{
            flashLightCompatibility(pos);
        }
        typedArray.recycle();
    }

    /**
     * Used to show AlertDialog, informs that flashlight is not available on this device
     */
    private void showNoFlashError(){
        AlertDialog alert = new AlertDialog.Builder(instance)
                .create();
        alert.setTitle(instance.getString(R.string.flashlight_error_title));
        alert.setMessage(instance.getString(R.string.flashlight_error_text));
        alert.setButton(DialogInterface.BUTTON_POSITIVE, instance.getString(R.string.dialog_ok), (dialog, which) -> dialog.cancel());
        alert.show();
    }
}
