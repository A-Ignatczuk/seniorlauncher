package pl.ignatczuk.seniorlauncher.utils;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceActivity;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

import pl.ignatczuk.seniorlauncher.R;
import pl.ignatczuk.seniorlauncher.activities.HomeScreenActivity;
import pl.ignatczuk.seniorlauncher.activities.SettingsActivity;

/**
 * Responsible for sos functionality
 */
public class SosUtil {

    private HomeScreenActivity instance;

    //helper strings
    private static final String SOS_NUM = "sos_number";
    private static final String SOS_MODE = "sos_mode";
    private static final String SOS_NAME = "sos_name";
    private static final String SOS_SWITCH = "sos_switch";

    //required permissions list
    private static String[] sosPermissions = {Manifest.permission.CALL_PHONE, Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE};

    public SosUtil(HomeScreenActivity instance){
        this.instance = instance;
    }

    /**
     * Shows alertDialog when sos functionality misses configuration
     * @param dialog previous alertDialog, necessary because should be closed before new alertDialog is created
     */
    private void sosMisConfigured(DialogInterface dialog){
        dialog.dismiss();
        new AlertDialog.Builder(instance)
                .setTitle(R.string.sos_config_title)
                .setMessage(R.string.sos_config_message)
                .setPositiveButton(R.string.question_positive, (dialog1, which1) -> {
                    Intent intent = new Intent(instance, SettingsActivity.class);
                    intent.putExtra(PreferenceActivity.EXTRA_SHOW_FRAGMENT, SettingsActivity.GeneralPreferenceFragment.class.getName());
                    dialog1.dismiss();
                    instance.startActivity(intent);
                })
                .setNegativeButton(R.string.question_negative, (dialog1, which1) -> dialog1.dismiss())
                .show();
    }

    /**
     * Performs SOS actions depending on preference
     * @param dialog dialog variable so that could be dismissed
     */
    private void sosWorker(DialogInterface dialog){
        String message = "SOS! " + instance.getPrefs().getString(SOS_NAME, "") + " " + instance.getString(R.string.sos_sms_text);
        if (Objects.equals(instance.getPrefs().getString(SOS_MODE, "1"), "0")){
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + instance.getPrefs().getString(SOS_NUM, "")));
            instance.startActivity(intent);
            Toast.makeText(instance, instance.getString(R.string.sos_call_toast) + " " + instance.getPrefs().getString(SOS_NUM, ""), Toast.LENGTH_LONG).show();
            dialog.dismiss();
        }
        if (Objects.equals(instance.getPrefs().getString(SOS_MODE, "1"), "1")){
            SmsManager smsManager = SmsManager.getDefault();
            ArrayList<String> dividedMessage = smsManager.divideMessage(message);
            smsManager.sendMultipartTextMessage(instance.getPrefs().getString(SOS_NUM, ""), null, dividedMessage, null, null);
            Toast.makeText(instance, instance.getString(R.string.sos_sms_toast) + " " + instance.getPrefs().getString(SOS_NUM, ""), Toast.LENGTH_LONG).show();
            dialog.dismiss();
        }
    }

    /**
     * Shows alertDialog to confirm SOS action
     */
    private void sosHelper(){
        new AlertDialog.Builder(instance)
                .setTitle(R.string.sos_question_title)
                .setMessage(R.string.sos_question_message)
                .setPositiveButton(R.string.question_positive, (dialog, which) -> {
                    if ((instance.getPrefs().getString(SOS_NUM, "") != null && Objects.requireNonNull(instance.getPrefs().getString(SOS_NUM, "")).isEmpty()) || (instance.getPrefs().getString(SOS_NAME, "") != null && Objects.requireNonNull(instance.getPrefs().getString(SOS_NAME, "")).isEmpty() && Objects.equals(instance.getPrefs().getString(SOS_MODE, "1"), "1"))){
                        sosMisConfigured(dialog);
                    }
                    else {
                        sosWorker(dialog);
                    }
                })
                .setNegativeButton(R.string.question_negative, (dialog, which) -> dialog.dismiss())
                .show();
    }

    /**
     * Handles the entire SOS functionality
     */
    public void sosMainFunction(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (PermissionsChecker.checkPermissions(instance, sosPermissions) && instance.getPrefs().getBoolean(SOS_SWITCH, false)){
                sosHelper();
            }
            else{
                Toast.makeText(instance, instance.getString(R.string.sos_disabled_toast), Toast.LENGTH_LONG).show();
            }
        }
        else{
            if (instance.getPrefs().getBoolean(SOS_SWITCH, false)){
                sosHelper();
            }
            else{
                Toast.makeText(instance, instance.getString(R.string.sos_disabled_toast), Toast.LENGTH_LONG).show();
            }
        }

    }
}
