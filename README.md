**PLEASE SET YOUR SDK PATH FOR THE PROJECT FIRST BEFORE CONTINUE. YOU CAN DO THIS WITH `setSdkPath.sh` FILE**

## How to use Gradle from terminal

You can execute all the build tasks available to your Android project using the Gradle wrapper command line tool. It's available as a batch file for Windows (`gradlew.bat`) and a shell script for Linux and Mac (`gradlew.sh`), and it's accessible from the root of each project you create with Android Studio.

* On Windows :
```gradlew task-name```

* On Linux or Mac:
```./gradlew task-name```

To see a list of all available build tasks for your project, execute `tasks`:
```gradlew tasks```

For example, you can build project by executing:
```gradlew build```

If build failed because of lint warnings, you can force build by executing:
```gradlew build -x lint```

---

## Build and deploy APK

For immediate app testing and debugging, you can build a debug APK. The debug APK is signed with a debug key provided by the SDK tools and allows debugging through `adb`.

To build a debug APK, open a command line and navigate to the root of your project directory. To initiate a debug build, invoke the `assembleDebug` task:

```gradlew assembleDebug```

This creates an APK named module_name-debug.apk in `project_name/module_name/build/outputs/apk/`. The file is already signed with the debug key and aligned with `zipalign`, so you can immediately install it on a device.

Or to build the APK and immediately install it on a running emulator or connected device, instead invoke `installDebug`:

```gradlew installDebug```

---

## Deploy an app to a physical device

Before you can run your app on a device, you must enable **USB debugging** on your device. You can find the option under **Settings > Developer options**.

Once your device is set up and connected via USB, you can install your app using either the Gradle install tasks or the adb tool:

```adb -d install path/to/your_app.apk```

All APKs you build are saved in `project_name/module_name/build/outputs/apk/`.